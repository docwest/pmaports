# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=device-samsung-serranovelte
pkgdesc="Samsung Galaxy S4 Mini Value Edition"
pkgver=4
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="armv7"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo rootston.ini"
subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
	$pkgname-phosh
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/WiFi/BT/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-samsung-serranovelte-venus firmware-samsung-serranovelte-wcnss"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-samsung-serranovelte-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

phosh() {
	install_if="$pkgname postmarketos-ui-phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

sha512sums="53496eed0ba8eee3a20940845222c1a011c69ea00d4cd2999709956e439ef53686524efe8406aa2e8cf3f2f0289e6293e329a83b006a88004a5cfc2f0f1cb7a5  deviceinfo
94c866b6583faadc4a96a4d737983ba7838ede52afa5e29e261ef0ad0f2afe29fd3b793c9208ae74c7d48db6b991ad21800b9e457fbba4c69ce9dec2cda268ea  rootston.ini"
